import { Routes, RouterModule} from "@angular/router";
import { NgModule } from '@angular/core';


const APP_ROUTES: Routes = [
   /*  {path: 'login', component: LoginComponent}, */
    {path:'**', pathMatch: 'full', redirectTo: 'home'}
]; 


@NgModule({
    imports: [RouterModule.forRoot(APP_ROUTES)],
    exports: [RouterModule]
  })
  export class AppRoutingModule { }